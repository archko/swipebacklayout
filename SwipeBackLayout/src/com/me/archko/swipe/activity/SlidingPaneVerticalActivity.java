package com.me.archko.swipe.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.VerticalSlidingPaneLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.me.archko.swipe.R;
import com.me.archko.swipe.Shakespeare;

/**
 * This example illustrates a common usage of SlidingPaneLayout in the Android support library.
 *
 * <p>A SlidingPaneLayout should be positioned at the top of your view hierarchy, placing it
 * below the action bar but above your content views. It is ideal as a two-pane layout
 * for larger screens, used in place of a horizontal LinearLayout.</p>
 *
 * <p>What separates SlidingPaneLayout from LinearLayout in this usage is that SlidingPaneLayout
 * allows these wide, two-pane layouts to overlap when horizontal space is at a premium. The user
 * can then access both panes by physically sliding the content pane into view or out of the way
 * or implicitly by moving focus between the two panes. This can greatly simplify development
 * of Android apps that support multiple form factors and screen sizes.</p>
 *
 * <p>When it comes to your navigation hierarchy, the left pane of a SlidingPaneLayout is always
 * considered to be one level up from the right content pane. As such, your Action Bar's
 * Up navigation should be enabled if the right pane is obscuring the left pane, and invoking it
 * should open the panes, revealing the left pane for normal interaction. From this open state
 * where the left pane is in primary focus, the Action Bar's Up affordance should act as if
 * both panes were fully visible in the activity window and navigate to the activity one level up
 * in the app's logical hierarchy. If the activity is the root of the application's task, the up
 * affordance should be disabled when the sliding pane is open and showing the left pane.
 * This code example illustrates this root activity case.</p>
 *
 * <p>Note that SlidingPaneLayout differs in usage from DrawerLayout. While DrawerLayout offers
 * sliding utility drawers for extended navigation options and actions, the panes of a
 * SlidingPaneLayout are firmly part of the content itself. If it would not make sense for
 * both panes to be visible all the time on a sufficiently wide screen, DrawerLayout and its
 * associated patterns are likely to be a better choice for your usage.</p>
 */
public class SlidingPaneVerticalActivity extends Activity {

    private VerticalSlidingPaneLayout mSlidingLayout;
    private ListView mList;
    private TextView mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sliding_vertical_main);

        mSlidingLayout=(VerticalSlidingPaneLayout) findViewById(R.id.pane);
        mList = (ListView) findViewById(R.id.left_pane);
        mContent=(TextView) findViewById(R.id.info_text);

        //mSlidingLayout.setParallaxDistance(400);
        mSlidingLayout.closePane();

        mList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                Shakespeare.TITLES));
        mList.setOnItemClickListener(new ListItemClickListener());

        //mSlidingLayout.getViewTreeObserver().addOnGlobalLayoutListener(new FirstLayoutListener());
    }

    /**
     * This list item click listener implements very simple view switching by changing
     * the primary content text. The slider is closed when a selection is made to fully
     * reveal the content.
     */
    private class ListItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mContent.setText(Shakespeare.DIALOGUE[position]);
            //mSlidingLayout.smoothSlideClosed();
        }
    }
}
