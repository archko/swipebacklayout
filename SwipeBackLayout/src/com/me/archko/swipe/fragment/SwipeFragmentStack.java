package com.me.archko.swipe.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.me.archko.swipe.R;
import me.imid.swipebacklayout.lib.SwipeBackLayout;

/**
 * @author archko
 */
public class SwipeFragmentStack extends FragmentActivity {

    public static final String TAG="FragmentStack";
    int mStackLevel=1;
    private SwipeBackLayout mSwipeBackLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe_fragment_stack);
        mSwipeBackLayout=(SwipeBackLayout) findViewById(R.id.swipe);
        mSwipeBackLayout.setEdgeTrackingEnabled(SwipeBackLayout.EDGE_ALL);
        mSwipeBackLayout.addSwipeListener(new SwipeBackLayout.SwipeListener() {
            @Override
            public void onScrollStateChange(int state, float scrollPercent) {
                Log.v(TAG, "onScrollStateChange:"+state+" scrollPercent:"+scrollPercent);
                if (state==SwipeBackLayout.STATE_IDLE&&scrollPercent>1) {
                    popTop(false);
                }
            }

            @Override
            public void onEdgeTouch(int edgeFlag) {
                //vibrate(VIBRATE_DURATION);
            }

            @Override
            public void onScrollOverThreshold() {
                //vibrate(VIBRATE_DURATION);
            }
        });

        // Watch for button clicks.
        Button button=(Button) findViewById(R.id.new_fragment);
        button.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                addFragmentToStack();
            }
        });
        button=(Button) findViewById(R.id.home);
        button.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // If there is a back stack, pop it all.
                FragmentManager fm=getSupportFragmentManager();
                if (fm.getBackStackEntryCount()>0) {
                    fm.popBackStack(fm.getBackStackEntryAt(0).getId(),
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }
        });
        button=(Button) findViewById(R.id.delete_fragment);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popTop(true);
            }
        });

        getSupportFragmentManager().addOnBackStackChangedListener(backStackChangedListener);

        if (savedInstanceState==null) {
            // Do first time initialization -- add initial fragment.
            Fragment newFragment;
            newFragment= getSupportFragmentManager().findFragmentById(R.id.simple_fragment);
            if (null==newFragment) {
                newFragment=newInstance(mStackLevel);
                FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
                ft.add(R.id.simple_fragment, newFragment).commit();
            } else {
                Log.d(TAG, "old:"+newFragment);
            }
        } else {
            mStackLevel=savedInstanceState.getInt("level");
            Fragment newFragment;
            newFragment= getSupportFragmentManager().findFragmentById(R.id.simple_fragment);
            Log.d(TAG, "old2:"+newFragment);
        }
    }

    FragmentManager.OnBackStackChangedListener backStackChangedListener=new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            Log.d(TAG, "onBackStackChanged");
            FragmentManager fm=getSupportFragmentManager();
            for (int i=0;i<fm.getBackStackEntryCount();i++) {
                FragmentManager.BackStackEntry entry=fm.getBackStackEntryAt(i);
                Log.d(TAG, "back entry:"+entry);
            }
            int cc=mSwipeBackLayout.getChildCount();
            for (int i=0;i<cc;i++) {
                Log.d(TAG, "child:"+i+" >"+mSwipeBackLayout.getChildAt(i));
            }
        }
    };

    private void popTop(boolean remove) {
        if (remove) {
            removeView();
        }
        FragmentManager fm=getSupportFragmentManager();
        Log.d(TAG, "popTop:"+fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount()>0) {
            fm.popBackStackImmediate(fm.getBackStackEntryAt(fm.getBackStackEntryCount()-1).getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

            mStackLevel--;
        } else {
            finish();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("level", mStackLevel);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //FragmentManager fm=getSupportFragmentManager();
        //Log.d(TAG, "onBackPressed:"+fm.getBackStackEntryCount());
        popTop(true);
    }

    void addFragmentToStack() {
        mStackLevel++;

        // Instantiate a new fragment.
        Fragment newFragment=newInstance(mStackLevel);

        // Add the fragment to the activity, pushing this transaction
        // on to the back stack.
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
        //ft.setCustomAnimations(R.anim.anim_enter_right, R.anim.anim_leave_left);
        //ft.setCustomAnimations(R.anim.anim_enter_right, R.anim.anim_leave_left, R.anim.anim_enter_left2, R.anim.anim_leave_right);
        //ft.setCustomAnimations(R.anim.anim_enter_right, R.anim.anim_leave_left, R.anim.anim_enter_left, R.anim.anim_leave_right);
        ft.add(R.id.simple_fragment, newFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
        FragmentManager fm=getSupportFragmentManager();
        Log.d(TAG, "addFragmentToStack:"+fm.getBackStackEntryCount()+" mStackLevel:"+mStackLevel);
    }

    void removeView(){
        /*View view=mSwipeBackLayout.getChildAt(mSwipeBackLayout.getChildCount()-1);
        ViewGroup decor=(ViewGroup) view.getParent();
        decor.removeView(view);*/
        mSwipeBackLayout.removeTop();
    }

    private static int mBgIndex=0;

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    CountingFragment newInstance(int num) {
        CountingFragment f=new CountingFragment();

        // Supply num input as an argument.
        Bundle args=new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    public class CountingFragment extends Fragment {

        private int[] mBgColors;

        int mNum;

        private int[] getColors() {
            if (mBgColors==null) {
                Resources resource=getResources();
                mBgColors=new int[]{
                    resource.getColor(R.color.androidColorA),
                    resource.getColor(R.color.androidColorB),
                    resource.getColor(R.color.androidColorC),
                    resource.getColor(R.color.androidColorD),
                    resource.getColor(R.color.androidColorE),
                };
            }
            return mBgColors;
        }

        /**
         * When creating, retrieve this instance's number from its arguments.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mNum=getArguments()!=null ? getArguments().getInt("num") : 1;
            Log.d(TAG, "onCreate:"+this);
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            Log.v(TAG, "onViewCreated: parent:"+view.getParent());
            mSwipeBackLayout.attachToFragment(view);
        }

        /**
         * The Fragment's UI is just a simple text view showing its
         * instance number.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
            View v=inflater.inflate(R.layout.activity_demo, container, false);
            v.setBackgroundColor(getColors()[mBgIndex]);
            mBgIndex++;
            if (mBgIndex>=getColors().length) {
                mBgIndex=0;
            }
            Button button=(Button) v.findViewById(R.id.btn_start);
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    addFragmentToStack();
                }
            });
            TextView txtView=(TextView) v.findViewById(R.id.txt_num);
            txtView.setText(String.valueOf(mNum));

            return v;
        }

        @Override
        public void onResume() {
            super.onResume();
            Log.d(TAG, "onResume:"+this);
        }

        @Override
        public void onPause() {
            super.onPause();
            Log.d(TAG, "onPause:"+this);
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            Log.d(TAG, "onDestroyView:"+this);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            Log.d(TAG, "onDestroy:"+this);
        }
    }

}
